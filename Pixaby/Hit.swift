//
//  Hit.swift
//  Pixaby
//
//  Created by Brain Agency on 5/11/16.
//  Copyright © 2016 Test. All rights reserved.
//
import UIKit

class Hit: NSObject {

    var webformatURL:String?
    var previewURL:String?
    var tags:String?
    var user:String?

    init(withDictionary dictionary:[String:AnyObject]) {

        self.webformatURL = dictionary["webformatURL"] as? String
        self.previewURL = dictionary["previewURL"] as? String
        self.tags = dictionary["tags"] as? String
        self.user = dictionary["user"] as? String

    }

    class func parse_hits(data:AnyObject?) -> [Hit] {

        let parse_data = data as! [String:AnyObject]
        let hits_parse = parse_data["hits"] as! Array<[String:AnyObject]>

        var hits:[Hit] = []

        for data in hits_parse {

            hits.append(Hit(withDictionary: data))

        }

        return hits

    }

}