//
//  HitTableViewCell.swift
//  Pixaby
//
//  Created by Brain Agency on 5/11/16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit

class HitTableViewCell: UITableViewCell {

    @IBOutlet weak var hit_image_view:UIImageView!
    @IBOutlet weak var hit_tags:UILabel!
    @IBOutlet weak var hit_user:UILabel!

    override func awakeFromNib() {

        super.awakeFromNib()

    }

    func configureCell(hit:Hit){

        self.hit_tags.text = hit.tags
        self.hit_user.text = hit.user

        if let image_path = hit.previewURL {
            self.hit_image_view.setImageWithURL(NSURL(string: image_path)!, placeholderImage: nil)

        }
    }

}
