//
//  DetailCollectionViewCell.swift
//  Pixaby
//
//  Created by Brain Agency on 5/11/16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit

class DetailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var detail_image_view:UIImageView!

    override func awakeFromNib() {

        super.awakeFromNib()

    }

    override func prepareForReuse() {

        self.detail_image_view.image = nil
    }

    func configureCell(hit:Hit){

        if let path = hit.webformatURL {

            let request = NSURLRequest(URL: NSURL(string: path)!)
            self.detail_image_view.contentMode = .ScaleToFill
            let placeholder = UIImage(named: "placeholder")

            self.detail_image_view.setImageWithURLRequest(request, placeholderImage: placeholder, success: { (resuest:NSURLRequest, responce:NSHTTPURLResponse?, image:UIImage) in

                self.detail_image_view.contentMode = .ScaleAspectFit
                self.detail_image_view.image = image

                }, failure: { (resuest:NSURLRequest, responce:NSHTTPURLResponse?, error:NSError) in

                    self.detail_image_view.contentMode = .ScaleToFill
                    self.detail_image_view.image = placeholder
            })

        }
        
    }

}
