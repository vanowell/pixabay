//
//  ListTableViewController.swift
//  Pixaby
//
//  Created by Brain Agency on 5/11/16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import SVPullToRefresh

class ListTableViewController: UITableViewController {

    var result_data:[Hit] = []

    let per_page = 10
    var current_page:NSInteger = 1

    let search_query = "red+roses"

    // MARK : - reload data

    func loadData(using_pagination load_next_page:Bool) {

        current_page = load_next_page ? current_page + 1 : 1

        WebServiceController.shared.searchImages(search_query, page: current_page, per_page: per_page, successBlock: { (result) in

            if !load_next_page {
                self.result_data = []
                self.tableView.infiniteScrollingView.enabled = true
            }

            if result.count > 0{

                self.result_data += result
                self.tableView.reloadData()
                
            } else {

                self.tableView.infiniteScrollingView.enabled = false

            }

            self.tableView.pullToRefreshView.stopAnimating()
            self.tableView.infiniteScrollingView.stopAnimating()


        }) { (error) in
            
        }


    }

    // MARK: - View life cycle

    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.loadData(using_pagination: false)

        self.tableView.addPullToRefreshWithActionHandler { 

            self.loadData(using_pagination: false)

        }

        self.tableView.addInfiniteScrollingWithActionHandler { 

            self.loadData(using_pagination: true)

        }

    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        return 1

    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return result_data.count
        
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        return tableView.dequeueReusableCellWithIdentifier("list_cell", forIndexPath: indexPath)

    }

    // MARK: - Table view delegate

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

        let _cell = cell as! HitTableViewCell
        _cell.configureCell(result_data[indexPath.row])

    }

    // MARK: - prepare for segue

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        let vc = segue.destinationViewController as! DetailViewController
        vc.hits = self.result_data
        vc.current_row = self.tableView.indexPathForSelectedRow?.row

    }

}