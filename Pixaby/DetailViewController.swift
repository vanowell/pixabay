//
//  DetailViewController.swift
//  Pixaby
//
//  Created by Brain Agency on 5/11/16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit

class DetailViewController: UICollectionViewController {

    var hits:[Hit]?
    var current_row:NSInteger?

    var initialScrollDone = false

    //MARK : - view life cycle

    override func viewDidLoad() {

        super.viewDidLoad()

        self.collectionView?.registerNib(UINib(nibName: "DetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "detail_cell")

    }

    override func viewDidLayoutSubviews() {

        super.viewDidLayoutSubviews()

        if (!initialScrollDone) {

            initialScrollDone = true
            self.collectionView?.scrollToItemAtIndexPath(NSIndexPath(forRow: current_row!, inSection: 0), atScrollPosition: .None, animated: false)

        }

    }

    //MARK :- collection data source

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {

        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return (hits?.count)!
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        return collectionView.dequeueReusableCellWithReuseIdentifier("detail_cell", forIndexPath: indexPath)

    }

    //MARK :- collection delegate

    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {

        let _cell = cell as! DetailCollectionViewCell
        _cell.configureCell(hits![indexPath.row])

    }

    //MARK :- layout

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        return collectionView.frame.size
    }

}