//
//  WebServiceController.swift
//  Pixaby
//
//  Created by Brain Agency on 5/11/16.
//  Copyright © 2016 Test. All rights reserved.
//
import UIKit
import AFNetworking

class WebServiceController: AFHTTPSessionManager {

    let api_key = "2548191-683d370b5dd8b264fa399dbfd"
    let api_path = "https://pixabay.com/api/"

    static let shared:WebServiceController = WebServiceController()

    func searchImages(path:String, page:NSInteger, per_page:NSInteger, successBlock:(result:[Hit]) -> Void, errorBlock:(error:NSError?) -> Void){

        let params = ["key":api_key, "q" : path, "page" : page, "per_page" : per_page]

        self.GET(api_path, parameters: params, progress: nil, success: { (task:NSURLSessionDataTask, result:AnyObject?) in

            successBlock(result: Hit.parse_hits(result))

        }) { (task:NSURLSessionDataTask?, error:NSError) in

            errorBlock(error: error)

        }

    }

}